/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.krorawitt.finalproject.Testselection;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Stock;

public class StockDAO implements DAO_Interface<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Stock (StockName,StockAmount,StockPrice)VALUES (?,?,? );";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getStockName());
            stmt.setInt(2, object.getStockAmount());
            stmt.setDouble(3, object.getStockPrice());

            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT StockID,StockName,StockAmount,StockPrice FROM Stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int StockID = result.getInt("StockID");
                String StockName = result.getString("StockName");
                int StockAmount = result.getInt("StockAmount");
                double StockPrice = result.getDouble("StockPrice");
                Stock Stock = new Stock(StockID, StockName, StockAmount, StockPrice);
                list.add(Stock);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT StockID,StockName,StockAmount,StockPrice FROM Stock WHERE StockID = ?" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int StockID = result.getInt("StockID");
                String StockName = result.getString("StockName");
                int StockAmount = result.getInt("StockAmount");
                double StockPrice = result.getDouble("StockPrice");
                Stock Stock = new Stock(StockID, StockName, StockAmount, StockPrice);

                return Stock;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public int delete(int StockID) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Stock WHERE StockID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, StockID);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Stock SET StockName = ?,StockAmount = ?,StockPrice = ? WHERE StockID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getStockName());
            stmt.setInt(2, object.getStockAmount());
            stmt.setDouble(3, object.getStockPrice());
            stmt.setInt(4, object.getStockID());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        StockDAO dao = new StockDAO();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Stock(-1, "KafaeYen", 100, 500.00));
        System.out.println("id: " + id);
        Stock updateProduct = dao.get(id);
        System.out.println("updateproduct:" + updateProduct);

    }

}
