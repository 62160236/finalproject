/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.krorawitt.finalproject.Testselection;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author cherz__n
 */
public class UserDAO implements DAO_Interface<User> {

    @Override
    public int add(User object) {
        System.out.println("Add new user.");
        return 0;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT UserID,Name,Password FROM User";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int UserId = result.getInt("UserID");
                String Name = result.getString("Name");
                String Password = result.getString("Password");
                User User = new User(UserId, Name, Password);
                list.add(User);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT UserID,Name,Password FROM User WHERE UserID = ?" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int UserId = result.getInt("UserID");
                String Name = result.getString("Name");
                String Password = result.getString("Password");
                User User = new User(UserId, Name, Password);
                return User;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public int delete(int id) {
        System.out.println("Delete user.");
        return 0;
    }

    @Override
    public int update(User object) {
        System.out.println("Update user.");
        return 0;
    }

    public static void main(String[] args) {
        System.out.println("Add new user.");
    }

    ////////////////////////////////////////////////////////////////
    public boolean getUser(String username, String password) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT UserID,Name,Password FROM User";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                String Name = result.getString("Name");
                String Password = result.getString("Password");
                if (username.equals(Name) && password.equals(Password)) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }
}
