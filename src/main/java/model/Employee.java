/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author cherz__n
 */
public class Employee {

    private int EmpID;
    private String EmpName;
    private String EmpTelephone;
    private String EmpPosition;

    public int getEmpID() {
        return EmpID;
    }

    public void setEmpID(int EmpID) {
        this.EmpID = EmpID;
    }

    public String getEmpName() {
        return EmpName;
    }

    public void setEmpName(String EmpName) {
        this.EmpName = EmpName;
    }

    public String getEmpTelephone() {
        return EmpTelephone;
    }

    public void setEmpTelephone(String EmpTelephone) {
        this.EmpTelephone = EmpTelephone;
    }

    public String getEmpPosition() {
        return EmpPosition;
    }

    public void setEmpPosition(String EmpPosition) {
        this.EmpPosition = EmpPosition;
    }

    public Employee(int EmpID, String EmpName, String EmpTelephone, String EmpPosition) {
        this.EmpID = EmpID;
        this.EmpName = EmpName;
        this.EmpTelephone = EmpTelephone;
        this.EmpPosition = EmpPosition;
    }

    @Override
    public String toString() {
        return "emp{" + "EmpID=" + EmpID + ", EmpName=" + EmpName + ", EmpTelephone=" + EmpTelephone + ", EmpPosition=" + EmpPosition + '}';
    }

}
